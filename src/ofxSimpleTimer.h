#pragma once

//a simple timer class written by Dave Tennent
//
#include "ofMain.h"

class ofxSimpleTimer {
    public:
        void enable();
        void enable(float t);
        void disable();
        void update(ofEventArgs & args);
        ofEvent<string> TIMER_COMPLETE;

    private:
        float time_to_run;
        float time_of_start;
        
};
