
#include "ofxSimpleTimer.h"
        void ofxSimpleTimer::enable() 
        {
           enable(1.0f); 
        
        }
        void ofxSimpleTimer::enable(float t) {
            time_of_start = ofGetElapsedTimef();
            time_to_run = t;
            ofAddListener(ofEvents().update, this, &ofxSimpleTimer::update);
        }
        void ofxSimpleTimer::disable() {
            time_of_start = -999999.0f;
            ofRemoveListener(ofEvents().update, this, &ofxSimpleTimer::update);
        }
        void ofxSimpleTimer::update(ofEventArgs & args) {
            if(ofGetElapsedTimef() - time_of_start > time_to_run) {
                ofLogVerbose() << "TIMER: Timer Complete" << std::endl;
                string event_message = "TIMER_COMPLETE";
                ofNotifyEvent(TIMER_COMPLETE, event_message);
                disable();

                    
            }
        }




